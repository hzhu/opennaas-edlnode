package org.opennaas.itests;

/*
 * #%L
 * OpenNaaS :: iTests :: EDL Node Setup
 * %%
 * Copyright (C) 2007 - 2014 Fundació Privada i2CAT, Internet i Innovació a Catalunya
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import static org.openengsb.labs.paxexam.karaf.options.KarafDistributionOption.keepRuntimeFolder;
import static org.opennaas.itests.helpers.OpennaasExamOptions.includeFeatures;
import static org.opennaas.itests.helpers.OpennaasExamOptions.noConsole;
import static org.opennaas.itests.helpers.OpennaasExamOptions.opennaasDistributionConfiguration;
import static org.ops4j.pax.exam.CoreOptions.options;

import javax.inject.Inject;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.opennaas.core.resources.IResource;
import org.opennaas.core.resources.IResourceManager;
import org.opennaas.core.resources.ModelElementNotFoundException;
import org.opennaas.core.resources.ResourceException;
import org.opennaas.core.resources.capability.ICapability;
import org.opennaas.core.resources.protocol.IProtocolManager;
import org.opennaas.core.resources.descriptor.CapabilityDescriptor;
import org.opennaas.core.resources.descriptor.CapabilityProperty;
import org.opennaas.core.resources.descriptor.ResourceDescriptor;
import org.opennaas.core.resources.helpers.ResourceHelper;
import org.opennaas.core.resources.capability.CapabilityException;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.junit.Configuration;
import org.ops4j.pax.exam.junit.ExamReactorStrategy;
import org.ops4j.pax.exam.junit.JUnit4TestRunner;
import org.ops4j.pax.exam.spi.reactors.EagerSingleStagedReactorFactory;
import org.osgi.framework.BundleContext;
import org.opennaas.extensions.edl.node.capability.IPowerSetupCapability;
import org.opennaas.extensions.edl.node.model.edl.EDLNode;
import org.opennaas.extensions.edl.node.model.edl.Load;
import org.opennaas.extensions.edl.node.model.edl.Metric;
import org.opennaas.extensions.edl.node.model.edl.MonitorLog;
import org.opennaas.extensions.edl.node.model.edl.Unit;
import org.opennaas.extensions.edl.capability.monitor.capability.IPowerMonitorCapability;
import org.ops4j.pax.exam.util.Filter;
import org.osgi.service.blueprint.container.BlueprintContainer;


@RunWith(JUnit4TestRunner.class)
@ExamReactorStrategy(EagerSingleStagedReactorFactory.class)
public class EDLNodeMonitorTest {

	
	@Inject
	private BundleContext		bundleContext;

	@Inject
	protected IResourceManager	resourceManager;

	@Inject
	private IProtocolManager	protocolManager;
	
	private IResource sampleResource;
	private final static String	RESOURCE_TYPE			= "edlnode";
	private final static String	SAMPLE_CAPABILITY_TYPE	= "edl_node_setup";
	private final static String	CAPABILITY_IMPL_VERSION	= "1.0";
	private final static String	CAPABILITY_IMPL_NAME	= "dummy";
	
	private final static String	MON_SAMPLE_CAPABILITY_TYPE	= "edl_node_powermonitor";
	private final static String	MON_CAPABILITY_IMPL_VERSION	= "1.0";
	private final static String	MON_CAPABILITY_IMPL_NAME	= "edl";
	
	@Inject
	@Filter(value = "(osgi.blueprint.container.symbolicname=org.opennaas.extensions.edl.node)", timeout = 50000)
	private BlueprintContainer			nodeSetupBlueprintContainer;
	
	@Configuration
	public static Option[] configuration() {
		return options(opennaasDistributionConfiguration(),
				includeFeatures("opennaas-core","opennaas-edl-monitor", "itests-helpers"), // ADD ADITIONAL FEATURES
				noConsole(),
				keepRuntimeFolder());
	}
	
	@Test
	public void sampleTest() throws Exception{
		Assert.assertNotNull(sampleResource.getCapabilities());
		Assert.assertEquals(2, sampleResource.getCapabilities().size());

		ICapability capab = sampleResource.getCapabilities().get(0);
		Assert.assertTrue(capab instanceof IPowerSetupCapability);
		
		ICapability moncapab = sampleResource.getCapabilities().get(1);
		Assert.assertTrue(moncapab instanceof IPowerMonitorCapability);

		IPowerSetupCapability sampleCapability = (IPowerSetupCapability) capab;
		String greetings = sampleCapability.sayHello("OpenNaaS");
		Assert.assertEquals("Hello OpenNaaS", greetings);
		
		sampleCapability.createEnergySource("solar1");
		sampleCapability.setEnergySourcePrice("solar1", 2);
		sampleCapability.setEnergySourceEmission("solar1", 15);
		sampleCapability.setNodePowerMeter("vu", 3, 8);
		sampleCapability.setNodeOutlet(1, 1);
		sampleCapability.setPowerMeterDriver("vu", "127.0.0.1/16120");
		
		
		
		IPowerMonitorCapability monCapability = (IPowerMonitorCapability) moncapab;
		MonitorLog log = monCapability.getCurrentObservedLog();
		MonitorLog timelog = monCapability.getLogbyTime(30, 10);
		
		
		
		Assert.assertNotNull(monCapability.getCurrentObservedLog());
		Assert.assertNotNull(timelog);
		
		Assert.assertEquals(3, log.getEdlIncludeLoad().size());
		Assert.assertEquals(3, timelog.getEdlIncludeLoad().size());
		
		Assert.assertEquals(1, log.getEdlIncludeLoad().iterator().next().getEdlHasMeasurement().size());
		//Assert.assertEquals(140, log.getEdlIncludeLoad().iterator().next().getEdlHasMeasurement().iterator().next().getEdlTimestamp());
		//Assert.assertEquals(new Float(140), timelog.getEdlIncludeLoad().iterator().next().getEdlHasMeasurement().iterator().next().getEdlMetricvalue());
		
		sampleCapability.createMetric("EmissionEfficiency");
		sampleCapability.createMetric("TotalEmission");
		sampleCapability.deleteMetric("TotalEmission");
		
		IPowerMonitorCapability monCapability2 = (IPowerMonitorCapability) moncapab;
		MonitorLog log2 = monCapability2.getCurrentObservedLog();
		MonitorLog timelog2 = monCapability2.getLogbyTime(30, 10);
		
		Assert.assertNotNull(log2);
		Assert.assertNotNull(timelog2);
		
		Assert.assertEquals(4, log2.getEdlIncludeLoad().size());
		Assert.assertEquals(4, timelog2.getEdlIncludeLoad().size());
	}
	

	@Before
	public void prepareTest() throws ResourceException{
		startResource();
	}
	
	private void startResource() throws ResourceException {
		List<CapabilityDescriptor> lCapabilityDescriptors = new ArrayList<CapabilityDescriptor>();

		CapabilityDescriptor exampleCapabilityDescriptor = ResourceHelper.newCapabilityDescriptor(CAPABILITY_IMPL_NAME,
				CAPABILITY_IMPL_VERSION, SAMPLE_CAPABILITY_TYPE, "mock://user:pass@host.net:2212/mocksubsystem");
		lCapabilityDescriptors.add(exampleCapabilityDescriptor);

		
		CapabilityDescriptor monitorDescriptor = ResourceHelper.newCapabilityDescriptor(MON_CAPABILITY_IMPL_NAME,
				MON_CAPABILITY_IMPL_VERSION, MON_SAMPLE_CAPABILITY_TYPE, "mock://user:pass@host.net:2212/mocksubsystem");
		CapabilityProperty monitorProperty = new CapabilityProperty();
		monitorProperty.setName("edlnodeId");
		monitorProperty.setValue("node1");
		monitorDescriptor.getCapabilityProperties().add(monitorProperty);
		lCapabilityDescriptors.add(monitorDescriptor);
		
		ResourceDescriptor resourceDescriptor = ResourceHelper.newResourceDescriptor(lCapabilityDescriptors, RESOURCE_TYPE,
				"mock://user:pass@host.net:2212/mocksubsystem", "node1");
		
		sampleResource = resourceManager.createResource(resourceDescriptor);
		resourceManager.startResource(sampleResource.getResourceIdentifier());
	}
	
	@After
	public void revertTest() throws ResourceException{
		resourceManager.stopResource(sampleResource.getResourceIdentifier());
		resourceManager.removeResource(sampleResource.getResourceIdentifier());
	
	}
	
	
}
