package org.opennaas.extensions.edl.node.model.edl;

/*
 * #%L
 * OpenNaaS :: EDL :: NODE
 * %%
 * Copyright (C) 2007 - 2014 Fundació Privada i2CAT, Internet i Innovació a Catalunya
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.opennaas.extensions.edl.node.model.edl.Driver;
import org.opennaas.extensions.edl.node.model.edl.ObservedGMetric;
import org.opennaas.extensions.edl.node.model.edl.Outlet;

import java.util.Set;

//rdf("http://www.science.uva.nl/research/sne/edl.owl#PowerMeter")
public class PowerMeter extends HardwareSensorComponent {
	private Driver driver;
	private Set<Outlet> outlets;
	private Set<? extends ObservedGMetric> obmetrics;
	private String pduModel;
	private int numOfOutlets;
	private int numOfModules;
	private String pduName;
	
	//rdf("http://www.science.uva.nl/research/sne/edl.owl#hasDriver")
	public Driver getEdlHasDriver(){
		return driver;
	}
	public void setEdlHasDriver(Driver edlHasDriver){
		this.driver = edlHasDriver;
	}

	//rdf("http://www.science.uva.nl/research/sne/edl.owl#hasOutlet")
	public Set<Outlet> getEdlHasOutlet(){
		return outlets;
	}
	public void setEdlHasOutlet(Set<Outlet> edlHasOutlet){
		this.outlets = edlHasOutlet;
	}

	//rdf("http://www.science.uva.nl/research/sne/edl.owl#informOf")
	public Set<? extends ObservedGMetric> getEdlInformOf(){
		return obmetrics;
	}
	public void setEdlInformOf(Set<? extends ObservedGMetric> edlInformOf){
		this.obmetrics = edlInformOf;
	}

	//rdf("http://www.science.uva.nl/research/sne/edl.owl#model")
	public String getEdlModel(){
		return pduModel;
	}
	public void setEdlModel(String edlModel){
		this.pduModel = edlModel;
	}

	//rdf("http://www.science.uva.nl/research/sne/edl.owl#numberOfOutlet")
	public Integer getEdlNumberOfOutlet(){
		return numOfOutlets;
	}
	public void setEdlNumberOfOutlet(Integer edlNumberofoutlet){
		this.numOfOutlets = edlNumberofoutlet;
	}
	
	//rdf("http://www.science.uva.nl/research/sne/edl.owl#numberOfModule")
	public Integer getEdlNumberOfModule(){
		return numOfModules;
	}
	public void setEdlNumberOfModule(Integer edlNumberOfModule){
		this.numOfModules = edlNumberOfModule;
	}

	public String getEdlPDUName(){
		return pduName;
	}
	public void setEdlPDUName(String edlPDUName){
		this.pduName = edlPDUName;
	}
}
