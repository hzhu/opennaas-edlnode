package org.opennaas.extensions.edl.node.model.edl;

/*
 * #%L
 * OpenNaaS :: EDL :: NODE
 * %%
 * Copyright (C) 2007 - 2014 Fundació Privada i2CAT, Internet i Innovació a Catalunya
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.opennaas.extensions.edl.node.model.edl.EnergySource;
import org.opennaas.extensions.edl.node.model.edl.MonitorLog;
import org.opennaas.extensions.edl.node.model.edl.Outlet;
import org.opennaas.extensions.edl.node.model.edl.PowerCapability;
import org.opennaas.extensions.edl.node.model.edl.PowerMeter;
import org.opennaas.extensions.edl.node.model.edl.PowerState;

import java.util.Set;


//rdf("http://schemas.ogf.org/nml/2013/05/base#Node")
public class EDLNode{
	private PowerState powerstate;
	private Outlet outlet;
	private Set<PowerCapability> powercapabilities;
	private MonitorLog monitorlog;
	private PowerMeter pdu;
	private String nodename;
	private Set<EnergySource> energysources;
	private String				id;
	
	//rdf("http://www.science.uva.nl/research/sne/edl.owl#atPowerState")
	public PowerState getEdlAtPowerState(){
		return powerstate;
	}
	public void setEdlAtPowerState(PowerState edlAtPowerState){
		this.powerstate = edlAtPowerState;
	}

	//rdf("http://www.science.uva.nl/research/sne/edl.owl#attachTo")
	public Outlet getEdlAttachTo(){
		return outlet;
	}
	public void setEdlAttachTo(Outlet edlAttachTo){
		this.outlet = edlAttachTo;
	}

	//rdf("http://www.science.uva.nl/research/sne/edl.owl#hasCapability")
	public Set<PowerCapability> getEdlHasCapability(){
		return powercapabilities;
	}
	public void setEdlHasCapability(Set<PowerCapability> edlHasCapability){
		this.powercapabilities = edlHasCapability;
	}

	//rdf("http://www.science.uva.nl/research/sne/edl.owl#hasLog")
	public MonitorLog getEdlHasLog(){
		return monitorlog;
	}
	public void setEdlHasLog(MonitorLog edlHasLog){
		this.monitorlog = edlHasLog;
	}

	//rdf("http://www.science.uva.nl/research/sne/edl.owl#monitoredBy")
	public PowerMeter getEdlMonitoredBy(){
		return pdu;
	}
	public void setEdlMonitoredBy(PowerMeter powermeter){
		this.pdu = powermeter;
	}

	//rdf("http://www.science.uva.nl/research/sne/edl.owl#nodename")
	public String getEdlNodename(){
		return nodename;
	}
	public void setEdlNodename(String edlNodename){
		this.nodename = edlNodename;
	}

	//rdf("http://www.science.uva.nl/research/sne/edl.owl#useEnergySource")
	public Set<EnergySource> getEdlUseEnergySource(){
		return energysources;
	}
	public void setEdlUseEnergySource(Set<EnergySource> edlUseEnergySource){
		this.energysources = edlUseEnergySource;
	}
	
	
	/**
	 * //return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * //param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	
	
}