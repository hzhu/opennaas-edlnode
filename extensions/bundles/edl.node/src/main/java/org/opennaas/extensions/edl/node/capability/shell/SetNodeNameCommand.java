package org.opennaas.extensions.edl.node.capability.shell;

/*
 * #%L
 * OpenNaaS :: EDL :: NODE
 * %%
 * Copyright (C) 2007 - 2014 Fundació Privada i2CAT, Internet i Innovació a Catalunya
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.apache.felix.gogo.commands.Argument;
import org.apache.felix.gogo.commands.Command;
import org.opennaas.core.resources.IResource;
import org.opennaas.core.resources.shell.GenericKarafCommand;
import org.opennaas.extensions.edl.node.capability.IPowerSetupCapability;

@Command(scope = "edl", name = "setNodeName", 
description = "Set the name of the resource")
public class SetNodeNameCommand extends GenericKarafCommand{

	@Argument(index = 0, name = "resourceType:resourceName", 
			description = "The resource id", required = true, multiValued = false)
	private String	resourceName;

	@Argument(index = 1, name = "nodeName", description = "The name of the resource.", 
			required = true, multiValued = false)
	private String	nodeName;


	@Override
	protected Object doExecute() throws Exception {
		printInitCommand("setNodeName");

		try {
			IResource resource = getResourceFromFriendlyName(resourceName);
			IPowerSetupCapability capab = (IPowerSetupCapability) resource
					.getCapabilityByInterface(IPowerSetupCapability.class);

			capab.setNodeName(nodeName);


		} catch (Exception e) {
			printError("Error in setNodeName in resource" + 
					resourceName + " with a name of " + nodeName);
			printError(e);
		} finally {
			printEndCommand();
		}
		printEndCommand();
		return null;
	}

}
