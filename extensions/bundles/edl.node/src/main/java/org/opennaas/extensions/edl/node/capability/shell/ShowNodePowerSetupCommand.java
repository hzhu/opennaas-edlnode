package org.opennaas.extensions.edl.node.capability.shell;

/*
 * #%L
 * OpenNaaS :: EDL :: NODE
 * %%
 * Copyright (C) 2007 - 2014 Fundació Privada i2CAT, Internet i Innovació a Catalunya
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.Set;

import org.apache.felix.gogo.commands.Argument;
import org.apache.felix.gogo.commands.Command;
import org.opennaas.core.resources.IResource;
import org.opennaas.core.resources.shell.GenericKarafCommand;
import org.opennaas.extensions.edl.node.model.edl.EDLNode;
import org.opennaas.extensions.edl.node.model.edl.EnergySource;

@Command(scope = "edl", name = "show", 
	description = "Show the setup info of a resource")
public class ShowNodePowerSetupCommand extends GenericKarafCommand{
	
	@Argument(index = 0, name = "resourceType:resourceName", 
			description = "The resource id", required = true, multiValued = false)
	private String	resourceName;


	@Override
	protected Object doExecute() throws Exception {
		printInitCommand("showNodeSetup");
		try {
			IResource resource = getResourceFromFriendlyName(resourceName);
			EDLNode node = (EDLNode) resource.getModel();
			Set<EnergySource> energySources = node.getEdlUseEnergySource();
			String esStr;
			if (energySources.isEmpty() && node.getEdlMonitoredBy().getEdlPDUName() == null) {
				esStr = "Energy source or power meter is defined!";
			}else {
				StringBuffer sb = new StringBuffer();
				sb.append("{");
				for (EnergySource es: energySources) {
					sb.append("EnergySource [id=" + es.getEdlEnergyName() + 
							"; price=" +  es.getEdlElectricityPrice() +
							"; emission=" + es.getEdlEmissionPerUnitofEnergy() + "],");
					
				}
				if (node.getEdlMonitoredBy().getEdlHasDriver() != null)
					sb.append("PDU [id=" + node.getEdlMonitoredBy().getEdlPDUName() +
							"; address: "+ node.getEdlMonitoredBy().getEdlHasDriver().getEdlURLaddress() + "],");
				else 
					sb.append("PDU [id=" + node.getEdlMonitoredBy().getEdlPDUName() +
							"; address:  ],");
				if (node.getEdlAttachTo() != null)
				sb.append("Outlet [id="+ node.getEdlAttachTo().getEdlModulenumber() + ":" +
						node.getEdlAttachTo().getEdlPortnumber() +"]");
				sb.append("}");
				esStr = sb.toString();
				printSymbol(esStr);
			}
			
		} catch (Exception e) {
			printError("Error in show node setup info " + resourceName );
			printError(e);
		} finally {
			printEndCommand();
		}
		printEndCommand();
		return null;
	}
}
