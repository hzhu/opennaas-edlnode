package org.opennaas.extensions.edl.node.repository;

/*
 * #%L
 * OpenNaaS :: EDL :: NODE
 * %%
 * Copyright (C) 2007 - 2014 Fundació Privada i2CAT, Internet i Innovació a Catalunya
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.LinkedHashSet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opennaas.core.resources.IModel;
import org.opennaas.core.resources.IResourceBootstrapper;
import org.opennaas.core.resources.Resource;
import org.opennaas.core.resources.ResourceException;
import org.opennaas.extensions.edl.node.model.edl.EnergySource;
import org.opennaas.extensions.edl.node.model.edl.PowerState;
import org.opennaas.extensions.edl.node.model.edl.PowerMeter;
import org.opennaas.extensions.edl.node.model.edl.Outlet;
import org.opennaas.extensions.edl.node.model.edl.PowerCapability;
import org.opennaas.extensions.edl.node.model.edl.MonitorLog;
import org.opennaas.extensions.edl.node.model.setupnode.PowerSetupNode;

public class PowerSetupResourceBootstrapper implements IResourceBootstrapper{
	Log				log	= LogFactory.getLog(PowerSetupResourceBootstrapper.class);

	private IModel	oldModel;

	@Override
	public void bootstrap(Resource resource) throws ResourceException {
		log.info("Loading bootstrap to start resource...");
		oldModel = resource.getModel();

		// Add here all the necessary methods to populate resource model

		resetModel(resource);

	}

	@Override
	public void resetModel(Resource resource) throws ResourceException {

		PowerSetupNode model = new PowerSetupNode();
		model.setId(resource.getResourceIdentifier().getId());
		model.setEdlNodename(resource.getResourceIdentifier().getId());
		model.setEdlAtPowerState(new PowerState());
		model.setEdlAttachTo(new Outlet());
		model.setEdlMonitoredBy(new PowerMeter());
		model.setEdlUseEnergySource(new LinkedHashSet<EnergySource>());
		model.setEdlHasCapability(new LinkedHashSet<PowerCapability>());
		model.setEdlHasLog(new MonitorLog());
		resource.setModel(model);
	}

	@Override
	public void revertBootstrap(Resource resource) throws ResourceException {

		resource.setModel(oldModel);
	}


}
