package org.opennaas.extensions.edl.capability.monitor.controller;

/*
 * #%L
 * OpenNaaS :: EDL ::PowerMonitor
 * %%
 * Copyright (C) 2007 - 2014 Fundació Privada i2CAT, Internet i Innovació a Catalunya
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.LinkedHashSet;
import java.util.Set;

import org.opennaas.core.resources.descriptor.CapabilityDescriptor;
import org.opennaas.extensions.edl.node.model.edl.Driver;
import org.opennaas.extensions.edl.node.model.edl.EDLNode;
import org.opennaas.extensions.edl.node.model.edl.Load;
import org.opennaas.extensions.edl.node.model.edl.Metric;
import org.opennaas.extensions.edl.node.model.edl.MonitorLog;
import org.opennaas.extensions.edl.node.model.edl.PowerMeter;
import org.opennaas.extensions.edl.node.model.edl.Outlet;
import org.opennaas.extensions.edl.node.model.edl.PowerConsumption;
import org.opennaas.extensions.edl.node.model.edl.PowerFactor;
import org.opennaas.extensions.edl.node.model.edl.EnergyConsumption;
import org.opennaas.extensions.edl.node.model.edl.Unit;
//import org.opennaas.extensions.edl.capability.monitor.Activator;
import org.opennaas.extensions.edl.node.Activator;

public class PowerMonitorController {
	public static MeasurementCollector create(String resourceId,
			String edlnodeId,  CapabilityDescriptor descriptor) throws Exception {
		MeasurementCollector collector = new MeasurementCollector();
		collector.setNode( (EDLNode) Activator.getResourceManagerService().getResource(
				Activator.getResourceManagerService().getIdentifierFromResourceName("edlnode",
						edlnodeId)).getModel());
		EDLNode node = collector.getNode();
		
		
		// use the default metrics and units
		if (node.getEdlHasLog().getEdlIncludeLoad() == null){
			
			MonitorLog log = new MonitorLog();
			Load powerLoad = new Load();
			PowerConsumption powerMetric = new PowerConsumption();
			powerMetric.setEdlHasMetricName("PowerConsumption");
			Unit powerUnit = new Unit();
			powerUnit.setEdlUnitname("Watt");
			powerMetric.setEdlHasUnit(powerUnit);
			powerLoad.setEdlMaptoMetric(powerMetric);
			
			Load energyLoad = new Load();
			EnergyConsumption energyMetric = new EnergyConsumption();
			energyMetric.setEdlHasMetricName("EnergyConsumption");
			Unit energyUnit = new Unit();
			energyUnit.setEdlUnitname("Wh");
			energyMetric.setEdlHasUnit(energyUnit);
			energyLoad.setEdlMaptoMetric(energyMetric);
			
			Load pfLoad = new Load();
			PowerFactor pfMetric = new PowerFactor();
			pfMetric.setEdlHasMetricName("PowerFactor");
			Unit pfUnit = new Unit();
			pfUnit.setEdlUnitname("%");
			pfMetric.setEdlHasUnit(pfUnit);
			pfLoad.setEdlMaptoMetric(pfMetric);
			
			Set<Load> loads = new LinkedHashSet<Load>();
			loads.add(powerLoad);
			loads.add(energyLoad);
			loads.add(pfLoad);
			log.setEdlIncludeLoad(loads);
			node.setEdlHasLog(log);
		}
			
		collector.setNode(node);
		
		return collector;
	}


}
