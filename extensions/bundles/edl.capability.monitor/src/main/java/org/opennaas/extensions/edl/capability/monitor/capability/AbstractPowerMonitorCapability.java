package org.opennaas.extensions.edl.capability.monitor.capability;

/*
 * #%L
 * OpenNaaS :: EDL ::PowerMonitor
 * %%
 * Copyright (C) 2007 - 2014 Fundació Privada i2CAT, Internet i Innovació a Catalunya
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.opennaas.core.resources.descriptor.CapabilityDescriptor;

public abstract class AbstractPowerMonitorCapability extends
	AbstractNotQueueingCapability{
	

	protected String edlnodeId;
	
	public AbstractPowerMonitorCapability(CapabilityDescriptor descriptor) {
		super(descriptor);
		this.edlnodeId = descriptor.getPropertyValue("edlnodeId");
	}

	
	public String getNodeId(){
		return edlnodeId;
	}
	
	public void setNodeId(String node){
		this.edlnodeId = node;
	}

}
