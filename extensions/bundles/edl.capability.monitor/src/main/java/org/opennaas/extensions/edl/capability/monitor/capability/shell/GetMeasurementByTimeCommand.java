package org.opennaas.extensions.edl.capability.monitor.capability.shell;

/*
 * #%L
 * OpenNaaS :: EDL ::PowerMonitor
 * %%
 * Copyright (C) 2007 - 2014 Fundació Privada i2CAT, Internet i Innovació a Catalunya
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

import org.apache.felix.gogo.commands.Argument;
import org.apache.felix.gogo.commands.Command;
import org.opennaas.core.resources.IResource;
import org.opennaas.core.resources.shell.GenericKarafCommand;
import org.opennaas.extensions.edl.capability.monitor.capability.IPowerMonitorCapability;
import org.opennaas.extensions.edl.node.model.edl.EDLNode;
import org.opennaas.extensions.edl.node.model.edl.EnergyConsumption;
import org.opennaas.extensions.edl.node.model.edl.Load;
import org.opennaas.extensions.edl.node.model.edl.Measurement;
import org.opennaas.extensions.edl.node.model.edl.MonitorLog;


@Command(scope = "edl", name = "getMeasurementByTime", 
	description = "Reads measurement of specified resource in calcaluted and observed green metrics in a time period")
public class GetMeasurementByTimeCommand extends GenericKarafCommand {

	@Argument(index = 0, name = "resourceType:resourceName", description = "The resource id to be monitored.",
			required = true, multiValued = false)
	private String		resourceId;

	@Argument(index = 1, name = "period", description = "The time duration of measurements is.",
			required = true, multiValued = false)
	private int		period;
	
	@Argument(index = 2, name = "interval", description = "The interval time of each measurement is.",
			required = true, multiValued = false)
	private int		interval;
	
	private DateFormat	dateFormat	= new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

	@Override
	protected Object doExecute() throws Exception {

		printInitCommand("getMeasurementByTime of resource: " + resourceId);
		try{
			IResource resource = getResourceFromFriendlyName(resourceId);
			MonitorLog log = ((IPowerMonitorCapability) resource.getCapabilityByInterface(IPowerMonitorCapability.class))
					.getLogbyTime(period, interval);
			EDLNode node = (EDLNode) resource.getModel();
			printMetrics(node, log);
		}catch (Exception e) {
			printError("Error in getMeasurementByTime of resource" + 
					resourceId);
			printError(e);
		}
		
		printEndCommand();
		return null;

	}

	private void printMetrics(EDLNode node, MonitorLog log) throws Exception{
		printSymbol("Green Metrics for resource " + resourceId);
		printSymbol(" Duration time: " + log.getEdlSampleDuration() + " sec; Sample inteval: " +
				log.getEdlSampleInterval() + " sec; ");
		Float energyValue = null;
		for (Load load: log.getEdlIncludeLoad()){
			//only deal with observed metrics
			if (load.getEdlHasMeasurement() != null) {
				Measurement firstMM = null;
				Iterator<Measurement> mmSet  =  load.getEdlHasMeasurement().iterator();
				Measurement lastMM = new Measurement();
				int firstLabel = 0;
				while (mmSet.hasNext()) {
					lastMM = mmSet.next();
					printSymbol(lastMM.getEdlMetricvalue() + load.getEdlMaptoMetric().getEdlHasUnit().getEdlUnitname());
					// get first measurement
					if (firstLabel == 0){
						firstMM = lastMM;
						firstLabel =1;
					}
				}
				
				if (load.getEdlMaptoMetric().getEdlHasMetricName().equals("EnergyConsumption") 
						|| load.getEdlMaptoMetric() instanceof EnergyConsumption) {
					energyValue = lastMM.getEdlMetricvalue() - firstMM.getEdlMetricvalue();
					Long timeValue = lastMM.getEdlTimestamp() - firstMM.getEdlTimestamp();
					energyValue = energyValue*3600;			
				}
			}
		}
		for(Load load: log.getEdlIncludeLoad()){
			
			if (load.getEdlMaptoMetric().getEdlHasMetricName().equals("EnergyEfficiency")){
				// unfinished, need to get throughput info of router; it will be done in future.
				throw new Exception("No performance metric now");
			}else if (load.getEdlMaptoMetric().getEdlHasMetricName().equals("EmissionEfficiency")){
				Float em = node.getEdlUseEnergySource().iterator().next().getEdlEmissionPerUnitofEnergy();
				printSymbol("Emission Efficiency: " + em + load.getEdlMaptoMetric().getEdlHasUnit().getEdlUnitname());
			}else if (load.getEdlMaptoMetric().getEdlHasMetricName().equals("TotalEmission")){
				Float te = energyValue*node.getEdlUseEnergySource().iterator().next().getEdlEmissionPerUnitofEnergy();
				printSymbol("Total Emission: " + te + load.getEdlMaptoMetric().getEdlHasUnit().getEdlUnitname());
			}else if (load.getEdlMaptoMetric().getEdlHasMetricName().equals("TotalElectricityCost")){
				Float cost = energyValue*node.getEdlUseEnergySource().iterator().next().getEdlElectricityPrice();
				printSymbol("Total Electricity Cost: " + cost + load.getEdlMaptoMetric().getEdlHasUnit().getEdlUnitname());
			}
		}
	}	
}

