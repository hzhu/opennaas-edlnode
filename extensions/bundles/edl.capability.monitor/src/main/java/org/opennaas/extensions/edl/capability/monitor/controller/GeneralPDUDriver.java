package org.opennaas.extensions.edl.capability.monitor.controller;

/*
 * #%L
 * OpenNaaS :: EDL ::PowerMonitor
 * %%
 * Copyright (C) 2007 - 2014 Fundació Privada i2CAT, Internet i Innovació a Catalunya
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.IOException;
import java.util.Set;

public interface GeneralPDUDriver {
	
	public long getCurrentTime(int moduleIndex, int outletIndex) throws IOException;
	
	public String getOutletName(int moduleIndex, int outletIndex) throws IOException;
	
	public String getModuleName(int moduleIndex, int outletIndex) throws IOException;
	
	public String getPower(int moduleIndex, int outletIndex) throws IOException;
	
	public String getEnergy(int moduleIndex, int outletIndex) throws IOException;
	
	public String getPowerFactor(int moduleIndex, int outletIndex) throws IOException;
	
	public String getNumOfOutlet(int moduleIndex, int outletIndex) throws IOException;
	
	public Set<String> getMeasurement(int moduleIndex, int outletIndex) throws IOException;
	
	public void powerOnOutlet(int moduleIndex, int outletIndex) throws IOException;
	
	public void powerOffOutlet(int moduleIndex, int outletIndex) throws IOException;

}
